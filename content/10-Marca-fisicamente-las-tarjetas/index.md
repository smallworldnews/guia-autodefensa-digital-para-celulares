#8.Marca físicamente - dibujar en- las tarjetas

Haz una marca a la SIM, tarjeta adicional de memoria, batería y teléfono con algo único y no rápidamente perceptible para una persona extraña.

Poner una cinta adhesiva sobre las partes que unen el teléfono te ayudará a identificar fácilmente si alguno de estos objetos han sido manipulados o remplazados.
