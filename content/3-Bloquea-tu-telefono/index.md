#1. Bloquea tu teléfono

##¿Qué significa?

Cambiar tus preferencias así el celular se bloquea después de un tiempo o cuando aprietes el botón de apagado y no se acceda a tu contenido rápidamente sin conocer el código.

##¿Cómo hacerlo?

Acceder a Ajustes o Configuración → Seguridad → Bloqueo de pantalla

Podes elegir entre:
• PATRÓN: será un determinado movimiento que tendrás que hacer con el dedo en la pantalla.
• PIN: un número de 4 cifras
• CONTRASEÑA: letras y números elegidos.

Si olvidas el código de bloqueo podes desbloquearlo con tu cuenta de gmail o resetear el móvil totalmente.
