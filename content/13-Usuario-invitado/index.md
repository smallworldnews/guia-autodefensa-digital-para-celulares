#11. Usuario invitado

Consiste en desvincular tu cuenta del teléfono eligiendo un perfil de usuario diferente al que usamos.

Este perfil de usuario invitadx tiene una configuración por defecto o sea una configuración de fábrica.

Es de gran utilidad cuando nos requisan el celular y nos piden acceder al mismo. Si accedemos a un perfil de invitadx no van a encontrar nuestra información personal. Por ende, sugerimos que crear algunos datos ficticios y creíbles dentro de este perfil.

##¿Cómo hacerlo?

1. Configuración o Ajustes
2. Usuarios
3. Invitado

![](images/13-usuario-invitado-guia-celulares.png)
