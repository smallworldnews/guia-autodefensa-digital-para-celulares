#Guía básica de autodefensa digital para celulares

![](images/portada-guia-celulares.png)


Esta es una guía básica para configurar nuestros celulares android de forma segura y un primer paso en la autodefensa digital.

Fue editado en el marco del 32º encuentro nacional de mujeres, trans, travestis y lesbianas en Chaco, Córdoba, Argentina en primavera del 2017.

Quienes lo realizaron creen que es necesario dar discusión sobre el cuidado en relación a las comunicaciones mediadas por la tecnología digital y a las estrategias que elegimos cuando hablamos de autodefensa feminista.

Aquí el enlace original de este fanzine- guía:
https://archive.org/details/guiaautodefensatelefonos
