#Créditos

**Enlace:**
https://archive.org/details/guiaautodefensatelefonos

**Contacto:**
Grupo en Riseup https://crabgrass.
riseup.net/cyberfeminismoscyberbrujeria
o envía un e-mail a: maho@riseup.net

**Licencia:**
Atribución-NoComercial-CompartirIgual 
CC BY-NC-SA

Esta licencia permite a otras remezclar, retocar, y crear a partir de su obra de forma no comercial, siempre y cuando den crédito y licencien sus nuevas creaciones bajo los mismos términos.
