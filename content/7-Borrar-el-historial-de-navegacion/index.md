#5. Borrar el historial de navegación

Borrar frecuentemente el historial de navegación de los buscadores desde sus ajustes.

##¿Cómo lo hago en Chrome?

1. Abrir chrome
2. Configuración
3. Historial
4. Borrar datos de navegación
5. Borrar datos

![](images/7-navegacion-guia-celulares.png)
