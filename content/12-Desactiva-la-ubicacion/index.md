#10. Desactiva la ubicación

De esta forma, aplicaciones como twitter, facebook y google no tendrán
acceso a nuestra ubicación exacta innecesariamente.

Actívala únicamente cuando lo desees o cuando uses aplicaciones de mapas.
