#2. Encripta tu celular

##¿Qué significa encriptar o cifrar el teléfono?

Es hacer que _**la información que hay dentro de tu dispositivo sea mucho menos accesible de lo habitual.**_ Esto se consigue haciendo que para llegar a cierta información que tengas almacenada tengas que introducir una contraseña, de forma que si el teléfono cae en otras manos, tengas cierta tranquilidad de que tus datos no estarán tan expuestos.

Una vez cifrado tu celular, la música, vídeos, fotos y datos de aplicaciones sólo serán accesibles si introduces la _**contraseña o el código PIN**_ que has configurado antes de iniciar el proceso.

##¿Cómo hacerlo?

En todas las versiones de Android el proceso es muy similar, puede variar la ruta en la que se encuentre esta opción, pero será muy similar independientemente de la marca de tu móvil.

Pasos:
1. Enchufar el teléfono, el proceso puede durar una hora.
2. Tener configurado un método de desbloqueo de la pantalla: tiene que ser pin o contraseña, no puede ser facial, ni patron, ni el simple deslizamiento.
3. Acceder a Ajustes o Configuración → Seguridad → Cifrar teléfono

![](images/4-encripta-guia-celulares.png)

4. Confirmar el PIN o contraseña y esperar a que el proceso termine, que puede tardar una hora aproximadamente, y no interrumpirlo. En caso de que el teléfono tenga una tarjeta microSD, los datos que contiene también serán cifrados, por lo que si la pasas a otro celular no podrás usarlos a no ser que primero lo desencriptes.

*Recomendación:*
Haz un respaldo de tus archivos y contactos antes de hacer el cifrado :) esto evitará que ante cualquier error pierdas tu información.
