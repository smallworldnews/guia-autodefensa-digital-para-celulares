#6. Asegurar control de acceso a aplicaciones

Colocar PIN de acceso a aplicaciones con la cual manejes información importante. Telegram cuenta con un sistema de seguridad para bloquear la aplicación con un código pin.

1. Acceder a Telegram
2. Ajustes
3. Privacidad y Seguridad
4. Código de acceso

![](images/8-acceso-apps.png)
