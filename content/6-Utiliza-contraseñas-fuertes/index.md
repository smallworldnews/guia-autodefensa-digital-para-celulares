#4. Utilizar contraseñas fuertes

Utiliza frases de paso, autenticación de dos pasos y contraseñas fuertes y diferentes para diferentes cuentas.

• **FRASES DE PASO:** Una frase que represente algo para nosotrxs, que sea larga, que incluya números, caracteres especiales y tanto mayúsculas como minúsculas.
Ejemplos: una forma de generar una contraseña sencilla de recordar y, aun así larga sería algo por el estilo:
“MiCuentaDe_Twitter_SeCreoEn2011”. Otra posibilidad es recurrir a algo más familiar “MiPrimerMovilFu€UnNokia3310” o “ElPisoEnElQueVivoEsUn3ero!”.

• **AUTENTICACIÓN DE DOS PASOS:** una vez habilitada, la aplicación nos pedirá la contraseña mas un método alternativo de autenticación, típicamente un código único enviado por SMS. Esto ofrece mayor protección a tu cuenta al demandar la comprobación de identidad por más de un método. Esto significa que, aunque alguien obtenga acceso a tu contraseña primaria, no podrá invadir tu cuenta a no ser que también posea tu teléfono móvil o otro medio secundario de autenticación.
