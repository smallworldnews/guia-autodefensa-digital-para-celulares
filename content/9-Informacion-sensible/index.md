#7. Protege la información sensible

Si es información sensible NO uses SMS ni llamadas convencionales.

Ya sea vía chat, llamada, video llamada o correo, utiliza canales que tengan _**cifrado de extremo a extremo**_ para comunicarte si se trata compartir información sensible; los medios convencionales son transparentes a cualquiera que quiera verlos.
