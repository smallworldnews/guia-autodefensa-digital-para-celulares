#Introducción

*"No tengo nada que ocultar en mi teléfono” . - Pues sí, todxs tenemos cosas que ocultar o por lo menos, deberíamos.-*

*Moxie Marlinspike*

Nuestros teléfonos no solo son ventanas abiertas en nuestras vidas, sino también en las vidas de nuestrxs amigxs, familias, compas... y cualquiera de nuestrxs contactos.

Quizás se piense que no hay nada para ocultar pero no podemos asegurar lo mismo por todxs los contactos con lxs que interactuamos por vía telefónica.

Proteger la información de nuestro teléfono es también proteger a nuestrxs amigxs.

Cuando hacemos una llamada telefónica o enviamos mensajes, se registran al menos la ubicación geográfica de quien llama y quien contesta, sus números de teléfono, la hora y duración de la comunicación y los números de serie de los dispositivos utilizados.

De igual manera pueden ser capturados los datos de nuestras comunicaciones por medio de alguna aplicación del smartphone. En un mismo aparato se vinculan los datos del número telefónico, correo, contactos, actividad en redes sociales, SMS, fotografías, archivos, datos de GPS y a veces la sincronización con otros dispositivos.

Hay que tener en cuenta que los métodos de espionaje estatales-empresariales se han perfeccionado y que nosotrxs participamos de manera activa en la maquinaria de vigilancia. Esta maquinaria no descansa y su funcionamiento es eficiente por definición. No es restrictiva sino silenciosa, no es reactiva sino retroactiva, no es solo individual y dirigida, sino también masiva.

Estamos convencidxs de que es momento de salir de la interpasividad impuesta para tener una participación activa y critica en cuanto a las formas y los medios que elegimos al comunicarnos. La seguridad y la privacidad de nuestros datos y comunicaciones son una construcción colectiva y una práctica de cuidado.

Más allá de las herramientas que utilicemos, lo fundamental para construir una comunicación segura es afianzar las prácticas colectivas de seguridad (lo que decimos, la forma y el momento en que lo decimos) siendo indispensable la participación y el compromiso de cada unx de nosotrxs.

![](images/guia-celulares-intro.png)
