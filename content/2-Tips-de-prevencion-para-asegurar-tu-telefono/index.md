## ¿Qué información tiene un teléfono de mi?

- Historial de llamadas, mensajes de texto enviados y recibidos, información de libretas de direcciones y calendarios.

- Fotos, videos, archivos de texto.
- Mails y acceso a redes sociales.
Conversaciones y grupos de Whatsapp y Telegram.
- Localización exacta mía en todo momento por medio de la red de telefonía y por medio del GPS.

Es importante que estemos al tanto de la información que se almacena, tanto en forma activa como pasiva, en el teléfono, ya sea en tu tarjeta SIM o en la memoria del teléfono. Estos datos revelan tu red de contactos y tu información personal.

En primer lugar, no hay que guardar información sensible en el teléfono. Si no tenés otra opción es mejor almacenar dicha información en tarjetas de memoria externas que puedan ser fácilmente desechadas en caso necesario – _**no coloques ningún detalle en la memoria interna del teléfono.**_

Quienes nos observan no necesitan saber con precisión lo que hemos dicho, pueden llegar a conclusiones solo analizando los _**metadatos**_ que nuestras comunicaciones producen durante un tiempo. Hay que tener siempre presente que los métodos de control y vigilancia digital y los métodos que tenemos para defendernos están en constante cambio y actualización por lo que debemos estar siempre atentxs y adecuar nuestros hábitos siendo cuidadosxs en el manejo y circulación de nuestra información sensible, pero sobre todo comprender que la autodefensa no es algo que podamos hacer individualmente sino que es una construcción colectiva.
