#3. Oculta tus mensajes

Previene que las aplicaciones de mensajería, como whatsapp o telegram, muestren el mensaje completo al recibir uno nuevo cuando tu teléfono está bloqueado.

##¿Cómo lo hago en whatsapp?

1. Ajustes o Configuración
2. Aplicaciones
3. WhatsApp
4. Notificaciones
5. Desactivar la opción “Permitir notificaciones” o “Permitir dar un vistazo” (y activar alguna opción que no muestre los mensajes en la pantalla principal como “Ocultar contenido confidencial” o “Bloquear Todos”).

![](images/5-oculta-mensajes-guia-celulares.png)

##¿Cómo lo hago en telegram?

1. Abrir Telegram
2. Configuración
3. Ajustes
4. Notificaciones y sonidos
5. Desactivar “Vista previa del mensaje”
