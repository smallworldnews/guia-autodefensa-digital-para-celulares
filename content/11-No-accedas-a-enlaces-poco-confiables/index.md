#9. No accedas a enlaces o archivos poco confiables que te envíen 

Es la principal forma de instalación de malware (software malicioso)

**Sigue estos consejos:**

- Evita abrir o darle clic:
a) Si recibes mensajes extraños a través de celular, SMS, correo electrónico o navegando en Internet.
b) Si desconoces el origen, la dirección o contacto que envía el mensaje y/o correo.

- Mantén al día las actualizaciones del sistema operativo y aplicaciones de tu celular.
